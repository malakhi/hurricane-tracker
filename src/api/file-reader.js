const parse = require('csv-parse');
const fs = require('fs-extra');
const minimist = require('minimist');
const { Storm, Observation } = require('../lib/storm');

const output = [];

const argv = minimist(process.argv.slice(2));
const input = fs.createReadStream(argv.f);
const parser = parse({ relax_column_count: true, trim: true }, (err, data) => {
  if (err) {
    console.log(err);
  } else {
    data.forEach((element, _index) => {
      if (element.length === 4) {
        output.push(new Storm(element));
      } else if (element.length === 21) {
        output[output.length - 1].observations.push(new Observation(element));
      } else {
        throw new Error(`Line ${_index + 1} of the input file is invalid.`);
      }
    });
  }
});

input.pipe(parser);
