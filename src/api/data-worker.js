const { MongoClient } = require('mongodb');
const fs = require('fs-extra');

const user = 'worker';
const pass = '1N#6&xguDy&&';
const url = 'mongodb://ds018248.mlab.com:18248/hurricane-tracker?ssl=true';
const dbName = 'hurricane-tracker';

MongoClient.connect(url,
  {
    auth: {
      user,
      password: pass,
    },
  }, (err, client) => {
    if (err) throw err;
    const db = client.db(dbName);
    client.close();
  });
