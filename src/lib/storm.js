const moment = require('moment');

class Storm {
  constructor([cycloneName, stormName, stormNumber]) {
    this.cyclone_name = cycloneName || '';
    this.storm_name = stormName || '';
    this.storm_number = stormNumber || null;
    this.start_date = null;
    this.end_date = null;
    this.observations = [];
  }
}

class Observation {
  constructor(data) {
    const sanitized = data.map((field) => {
      if (field === '-999') {
        return null;
      }
      return field;
    });
    this.date = moment.utc(`${sanitized[0]}T${sanitized[1]}`).toDate();
    this.record_identifier = sanitized[2]; // eslint-disable-line prefer-destructuring
    this.status = sanitized[3] || '';
    this.location = {
      type: 'Point',
      coordinates: [sanitized[4], sanitized[5]],
    };
    this.max_winds = sanitized[6]; // eslint-disable-line prefer-destructuring
    this.min_pressure = sanitized[7]; // eslint-disable-line prefer-destructuring
    this.radii = [
      sanitized[8],
      sanitized[9],
      sanitized[10],
      sanitized[11],
      sanitized[12],
      sanitized[13],
      sanitized[14],
      sanitized[15],
      sanitized[16],
      sanitized[17],
      sanitized[18],
      sanitized[19],
    ];
  }
}

module.exports = { Storm, Observation };
